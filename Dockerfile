from alpine
RUN apk update && apk upgrade && \
	apk add gcc alpine-sdk vim bash linux-lts linux-headers \
	linux-lts-dev git
RUN git clone https://github.com/gnab/rtl8812au /rtl8812au && \
	cd /rtl8812au && \
	make -j4 KVER=$(basename /lib/modules/*)

