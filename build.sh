#!/bin/sh
docker build -t alpine-rtl8812au422 .
# https://stackoverflow.com/questions/25292198/docker-how-can-i-copy-a-file-from-an-image-to-a-host
docker cp alpine-rtl8812au422:/rtl8812au/8812au.ko /mnt/temp2/lib/modules/5.4.34-0-lts/kernel/drivers/net/wireless/

